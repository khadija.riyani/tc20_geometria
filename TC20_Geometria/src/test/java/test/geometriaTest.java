package test;

import static org.junit.Assert.*;

import Geometria.geometria;

import org.junit.Test;

public class geometriaTest {

	//test cuadrado
	@Test
	public void TestAreaCuadrado(){
		
		int resultado = geometria.areacuadrado(9);
		int esperado = 81;
		assertEquals(esperado,resultado);
	}

	//test circulo
	@Test 
	public void TestareaCirculo() {
		
		double resultado = geometria.areaCirculo(2);
		double esperado = 12;
		double delta = 1;
		assertEquals(esperado,resultado,delta);
		
	}
	
	//test triangulo
	@Test 
	public void Testareatriangulo() {
		
		double resultado = geometria.areatriangulo(5,8);
		double esperado = 20;
		double delta = 1;
		assertEquals(esperado,resultado,delta);
		
	
	}
	
	//test rectangulo
	@Test 
	public void Testarearectangulo() {
		
		int resultado = geometria.arearectangulo(7,8);
		int esperado = 56;
		int delta = 1;
		assertEquals(esperado,resultado,delta);	
	
	}
	
	//test pentagono
	@Test 
	public void Testareapentagon() {
		
		int resultado = geometria.areapentagono(4,8);
		int esperado = 16;
		int delta = 1;
		assertEquals(esperado,resultado,delta);
		
	}
	
	//test rombo
	@Test 
	public void Testarearombo() {
		
		int resultado = geometria.arearombo(4,6);
		int esperado = 12;
		int delta = 1;
		assertEquals(esperado,resultado,delta);
		
	}
	
	//test romboide
	@Test 
	public void Testarearomboide() {
		
		int resultado = geometria.arearomboide(9,7);
		int esperado = 63;
		int delta = 1;
		assertEquals(esperado,resultado,delta);
		
	}
	
	//test trapecio
	@Test 
	public void Testareatrapecio() {
		
		int resultado = geometria.areatrapecio(9,3,7);
		int esperado = 12;
		int delta = 1;
		assertEquals(esperado,resultado,delta);
		
	}
	
	// get figura
	@Test 
	public void figura() {
		
		String resultado = geometria.figura(2);
		String esperado = "Circulo";
		int delta = 1;
		assertEquals(esperado,resultado,delta);
		
	}
	
	
	
	
	
	
	
	
}
